# Hello!


### to jest przykładowe rozwiązanie zadania dla PGSSoftWare 

---
#### co powinno być zrobione
> Projekt wykonaj wg. dołączonych makiet.

done

>  Przełączenie pomiędzy zakładkami powinno odbywać się bez przeładowania strony, w możliwie
najprostszy sposób.

done, używy angular-route
> Zastosuj czcionkę Roboto.

done, pobierana w sass'ie z google fonts

>  About Us – strona ze statycznym tekstem.

done

> Skicams – skorzystaj z JSON udostępnionego pod adresem:
https://makevoid-skicams.p.mashape.com/cams.json
podczas pobierania danych ustaw header requesta
X-Mashape-Key na wartość kxSXmUymofmshFHhhKxWOSJpqJsJp1I3zNnjsnqKwhITAiC1zw Wyświetl obrazki z dwóch kamerek z ośrodków narciarskich Andalo oraz Monte Bondone. Wyświetl aktualną datę w formacie DD-MM-YYYY.

done, (nie ma daty - zakładam że kiedyś również przychodziła z JSONa, teraz nie ma tam nic poza url'ami do zdjęć i nazwy)
> Contact – zbuduj prosty formularz, który zostanie zwalidowany po naciśnięciu przycisku Send. Pola Name oraz Email są wymagane, email powinien zostać wprowadzony w poprawnym formacie. Formularz nie powinien wysyłać żadnych danych. Panel z formularzem jest wyśrodkowany poziomo i pionowo.

done, walidacja html'a - nie było potrzeby robić własnej
> Stopka znajduje się zawsze przy dolnej krawędzi okna (lub pod treścią, jeżeli nie mieści się ona w przeglądarce).

done, choć do styli przywiązywałem mniej uwagi niż do JSa
> Strona powinna być responsywna – minimalna szerokość okna to 320px.

done, w podstawowy sposób, zastosowany bootstrap v3

Done

---
#### git repo
https://gitlab.com/okosowski/pgsSoftware

#### to download repo
```shell
cd go/to/dir
git clone https://gitlab.com/okosowski/pgsSoftware.git
```

#### to download all npm stuff
```shell
npm install
```

##### to start devel, watch all change
```shell
npm start
```
and go to **http://localhost:3000**

