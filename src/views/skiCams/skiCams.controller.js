class skiCamsController {
  constructor(dataStorage, $scope) {
    this.progress = true

    dataStorage.fetchCamsInfo().then((result) => {
      this.cams = result
      console.log('this.cams', this.cams)
      this.progress = false
      $scope.$digest()
    })

  }
}

export default skiCamsController

skiCamsController.$inject = ['dataStorage', '$scope']