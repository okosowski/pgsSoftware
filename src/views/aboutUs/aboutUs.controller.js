class aboutUsController {
  constructor(dataStorage) {
    this.name = 'aboutUsController'
    this.data = dataStorage.getAboutUsData()
  }
}

export default aboutUsController

aboutUsController.$inject = ['dataStorage']