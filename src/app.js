import angular from 'angular'
import ngRoute from 'angular-route'

import commons from './commons/commons'

import aboutCtrl from './views/aboutUs/aboutUs.controller'
import aboutTemp from './views/aboutUs/aboutUs.html'

import contactCtrl from './views/contact/contact.controller'
import contactTemp from './views/contact/contact.html'

import skiCamsCtrl from './views/skiCams/skiCams.controller'
import skiCamsTemp from './views/skiCams/skiCams.html'

import './styles.scss'

angular
  .module('pgsApp', [
    ngRoute,
    commons.name
  ])
  .config(function($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
      // .when('/aboutUs', {
      .when('/', {
        template: aboutTemp,
        controller:  aboutCtrl,
        controllerAs:  'ctrl'
      })
      .when('/skiCams', {
      // .when('/', {
        template: skiCamsTemp,
        controller: skiCamsCtrl,
        controllerAs:  'ctrl'
      })
      .when('/contact', {
      // .when('/', {
        template: contactTemp,
        controller:  contactCtrl,
        controllerAs:  'ctrl'
      })
      // .otherwise({redirectTo: '/aboutUs'})
      .otherwise({redirectTo: '/'})

    }
  )
