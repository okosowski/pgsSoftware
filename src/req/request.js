export function prepareRequest(url, method = 'GET', additionalHeaders = []) {
  let xhr = new XMLHttpRequest()
  xhr.open(method, url)
  if (additionalHeaders){
    for (let i = 0; i < additionalHeaders.length; i++){
      xhr.setRequestHeader(additionalHeaders[i].name, additionalHeaders[i].value)
    }
  }
  return xhr
}

export function addErrorHandlers(xhr, callback) {
  ['error', 'abort', 'timeout'].forEach((error) => {
    xhr.addEventListener(error, callback)
  })
}
