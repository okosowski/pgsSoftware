import angular from 'angular'
import dataStorageFactory from './dataStorage.factory'

let dataStorageModule = angular
  .module('pgsApp.common', [])
  .factory('dataStorage', dataStorageFactory)

export default dataStorageModule
