import forEach from 'lodash/forEach'

import { prepareRequest, addErrorHandlers } from '../../req/request'
import { makeResponse } from '../../req/response'

let dataStorageFactory = function () {

  const camsUrl = 'https://makevoid-skicams.p.mashape.com/cams.json'
  const camsAdditionalHeaders = {name: 'X-Mashape-Key', value: 'kxSXmUymofmshFHhhKxWOSJpqJsJp1I3zNnjsnqKwhITAiC1zw'}

  const aboutUsData = {
    title: 'Lorem Ipsum',
    text: 'Etiam ullamcorper. Suspendisse a pellentesque diui, non felis maecenas.'
  }

  let cams = {}

  let getAboutUsData = () => { return aboutUsData }

  let fetchCamsInfo = () => {
    let promise = new Promise((resolve, reject) => {
      let xhr = prepareRequest(camsUrl, 'GET', [camsAdditionalHeaders])
      xhr.addEventListener('load', () => {
        let response = makeResponse(xhr)
        return response.ok ? resolve(response) : reject(response)
      })
      addErrorHandlers(xhr, () => { reject(makeResponse(xhr)) })
      xhr.send()
    })
      .then((result) => {
        return Promise.resolve(convertResponse(result))
      })
      .catch((e) => {
        return Promise.reject(e)
      })

    return promise
  }

  let convertResponse = (result) => {
    if(!result){return false}
    let response = JSON.parse(result.response)

    let andalo = null
    let monteBondone = null

    forEach(response, (item) => {
      if (!!andalo && !!monteBondone) { return false } //for break loop
      if(item.name == 'Andalo'){
        andalo = convertSingleVenue(item)
      }else if (item.name == 'Monte Bondone'){
        monteBondone = convertSingleVenue(item)
      }
    })
    return {
      'andalo' : andalo,
      'monteBondone' : monteBondone
    }
  }

  let convertSingleVenue = (item) => {
    let result = {}
    result.name = item.name
    result.cams = []
    for (let i = 0; i < 2; i++){
      result.cams.push(item.cams[Object.keys(item.cams)[i]])
    }
    return result
  }

  return { getAboutUsData, fetchCamsInfo }
}

export default dataStorageFactory