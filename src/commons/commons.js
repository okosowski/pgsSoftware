import angular from 'angular'
import dataStorage from './dataStorage/dataStorage'

export default angular.module('pgsApp.commons', [
  dataStorage.name,
])
